// universe_waiter project universe_waiter.go
package main

import (
	"strconv"
	"strings"
	"universe_waiter/dispatcher"
)

var idispatcher dispatcher.Dispatcher

func main() {
	fsett := readConfig("config.yaml")
	laddr := ":" + strconv.Itoa(fsett.Port)
	switch strings.ToLower(fsett.Protocal) {
	case "rest":
		idispatcher = dispatcher.NewRESTDispatcher(fsett.Processors)
		startRESTServer(laddr)
	case "tcp":
		idispatcher = dispatcher.NewTCPDispatcher(fsett.Processors)
		startTCPServer(laddr)
	}
}
