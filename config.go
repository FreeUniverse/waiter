package main

import (
	"common/utils"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type settings struct {
	Port       int      `yaml:"port"`
	Protocal   string   `yaml:"protocal,omitempty"`
	Processors []string `yaml:"processors,omitempty"`
}

func defaultSettings() settings {
	s := settings{
		Port:     9999,
		Protocal: "REST",
		Processors: []string{
			"127.0.0.1:9998",
		},
	}
	return s
}

func readConfig(filename string) settings {
	s := defaultSettings()
	fd, err := os.Open(filename)
	if err != nil {
		return s
	}
	raw_data := utils.ReadData(fd)
	err = yaml.Unmarshal(raw_data, &s)
	if err != nil {
		log.Fatalf("解析配置文件錯誤。%s\n", err.Error())
	}
	return s
}
