Free Universe Waiter
====================

Free Universe的Waiter，用來溝通Viewer和Processor。

* 版本
    0.3.1
* 協議版本
    * TCP式
        0.3.1
    * REST式
        0.2.1

功能目標
--------
Viewer只知道Waiter的IP和端口，請求發與Waiter。
Waiter獲取到請求後，將請求分派給諸Processor，然後將結果匯總後返回給Waiter。
