package main

import (
	ds "common/data_structure"
	"log"
	"net"
)

func startTCPServer(laddr string) {
	listener, err := net.Listen("tcp", laddr)
	if err != nil {
		panic(err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("獲取Viewer連接錯誤：", err)
		}
		go handleConnection(conn)
	}
}

func handleConsultRequest(req ds.ConsultRequest) ds.ConsultResponse {
	var res ds.ConsultResponse
	for _, target := range req.Target {
		switch target {
		case "Planets":
			res.Planets = idispatcher.Planets()
		}
	}
	return res
}

func handleConnection(conn net.Conn) {
	defer conn.Close()
	req, err := ds.ParseMsg(conn)
	if err != nil {
		log.Println(err)
		return
	}
	switch req.(type) {
	case ds.UniverseRequest:
		info := idispatcher.Dispatch(req.(ds.UniverseRequest).Time, req.(ds.UniverseRequest).Planet)
		json_data := ds.ToMsg(ds.UniverseResponse(info))
		log.Printf(" 所取得universe: %s\n", json_data)
		conn.Write(json_data)
	case ds.ConsultRequest:
		json_data := ds.ToMsg(handleConsultRequest(req.(ds.ConsultRequest)))
		log.Printf(" 所有星球：%s\n", json_data)
		conn.Write(json_data)
	default:
		log.Println("未知類型傳入")
	}
}
