package dispatcher

import (
	ds "common/data_structure"
	"log"
	"net"
	"time"
)

type tcp_dispatcher struct {
	dispatcherBase
}

func NewTCPDispatcher(processors []string) *tcp_dispatcher {
	dispatcher := new(tcp_dispatcher)
	dispatcher.capacity = make(map[string][]string)
	dispatcher.processors = make(chan string, 10)
	dispatcher.planetInfos = make(map[string]ds.PlanetInfo)

	for _, processor := range processors {
		dispatcher.refresh(processor)
	}

	return dispatcher
}

func (d *tcp_dispatcher) refresh(remote string) { //TODO 先清除已有信息
	conn, err := net.DialTimeout("tcp", remote, 1000*1000*1000*0.3)
	if err != nil {
		return
	}
	defer conn.Close()
	var req ds.ConsultRequest
	req.Target = []string{"planets"}
	conn.Write(ds.ToMsg(req))

	res, err := ds.ParseMsg(conn)
	if err != nil {
		return
	}
	switch res.(type) {
	case ds.ConsultResponse:
		planetInfos := res.(ds.ConsultResponse).Planets
		log.Printf("[%s] :: %s\n", remote, planetInfos)
		planets := make([]string, len(planetInfos))
		for i, info := range planetInfos {
			planets[i] = info.Name
			_, has := d.planetInfos[info.Name]
			if !has { // 創建新的條目
				d.planetInfos[info.Name] = info
			} else { // 更新已有條目
			}
		}
		d.capacity[remote] = planets
		d.release(remote)
	}
}

func (d *tcp_dispatcher) getConn(name string) (net.Conn, string) {
	for {
		remote := d.demand(name)
		conn, err := net.Dial("tcp", remote)
		if err != nil {
			log.Printf("連接到Processor失敗。錯誤：%s\n", err.Error())
			go d.refresh(remote)
			time.Sleep(500000)
		} else {
			return conn, remote
		}
	}
	panic("不可達區域")
}

func (d *tcp_dispatcher) Dispatch(time string, planets []string) ds.Universe {
	getPlanet := func(name string, time string, seq chan ds.Planet) {
		conn, remote := d.getConn(name)
		defer func() {
			go func() {
				conn.Close()
				d.release(remote)
			}()
		}()
		req := ds.PlanetRequest{time, name}
		_, err := conn.Write(ds.ToMsg(req))
		if err != nil {
			log.Fatalln("發送請求失敗。錯誤：", err.Error())
		}

		res, err := ds.ParseMsg(conn)
		if err != nil {
			log.Fatalln("讀取回應失敗。錯誤：", err.Error())
		}
		tpinfo := res.(ds.PlanetResponse)
		seq <- tpinfo.Planet
	}

	return d.dispatcherBase._dispatch(time, planets, getPlanet)
}
