package dispatcher

import (
	ds "common/data_structure"
)

type dispatcherBase struct {
	processors  chan string
	capacity    map[string][]string      //map[IPAddr][]planets
	planetInfos map[string]ds.PlanetInfo //map[planet]PlanetInfo
}

func (d *dispatcherBase) release(processor string) {
	d.processors <- processor
}

func (d *dispatcherBase) demand(name string) string {
	var found bool
	for {
		found = false
		remote := <-d.processors
		for _, p := range d.capacity[remote] {
			if p == name {
				found = true
				break
			}
		}
		if found {
			return remote
		}
	}
	panic("不可達區域")
}

func (d dispatcherBase) Planets() []ds.PlanetInfo {
	planets := make([]ds.PlanetInfo, len(d.planetInfos))
	index := 0
	for _, info := range d.planetInfos {
		planets[index] = info
		index++
	}
	return planets
}

func (d dispatcherBase) PlanetList() []string {
	list := make([]string, len(d.planetInfos))
	i := 0
	for k, _ := range d.planetInfos {
		list[i] = k
		i++
	}
	return list
}

func (d dispatcherBase) Info(planet string) ds.PlanetInfo {
	return d.planetInfos[planet]
}

func (d *dispatcherBase) _dispatch(time string, planets []string, gp func(name string, time string, seq chan ds.Planet)) ds.Universe {
	seq := make(chan ds.Planet, 2)
	defer close(seq)

	for _, planet := range planets {
		go gp(planet, time, seq)
	}

	var universe ds.Universe
	universe.Time = time
	for len(universe.Planets) < len(planets) {
		universe.Planets = append(universe.Planets, <-seq)
	}
	return universe
}
