package dispatcher

import (
	ds "common/data_structure"
)

type Dispatcher interface {
	Planets() []ds.PlanetInfo
	PlanetList() []string
	Info(planet string) ds.PlanetInfo
	Dispatch(time string, planets []string) ds.Universe
}
