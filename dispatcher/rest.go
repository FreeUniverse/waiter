package dispatcher

import (
	ds "common/data_structure"
	"common/utils"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type rest_dispatcher struct {
	dispatcherBase
}

func NewRESTDispatcher(processors []string) *rest_dispatcher {
	dispatcher := new(rest_dispatcher)
	dispatcher.capacity = make(map[string][]string)
	dispatcher.processors = make(chan string, 10)
	dispatcher.planetInfos = make(map[string]ds.PlanetInfo)

	for _, processor := range processors {
		go dispatcher.refresh(processor)
	}

	return dispatcher
}

func (d *rest_dispatcher) refresh(remote string) {
	response, err := http.Get(fmt.Sprintf("http://%s/planet_list", remote))
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}
	var planets []string
	err = json.Unmarshal(utils.ReadData(response.Body), &planets)
	var info ds.PlanetInfo
	for _, name := range planets {
		response_info, err := http.Get(fmt.Sprintf("http://%s/info/%s", remote, name))
		if err != nil {
			log.Printf("%s\n", err.Error())
			return
		}
		err = json.Unmarshal(utils.ReadData(response_info.Body), &info)
		_, has := d.planetInfos[name]
		if !has { // 創建新的條目
			d.planetInfos[name] = info
		} else { // 更新已有條目
		}
	}

	//	var planetInfos []ds.PlanetInfo
	//	err = json.Unmarshal(utils.ReadData(response.Body), &planetInfos)
	//	if err != nil {
	//		log.Printf(err.Error())
	//		return
	//	}
	//	planets := make([]string, len(planetInfos))
	//	for i, info := range planetInfos {
	//		planets[i] = info.Name
	//	}

	d.capacity[remote] = planets
	d.release(remote)
}

func (d *rest_dispatcher) Dispatch(time string, planets []string) ds.Universe {
	getPlanet := func(name string, time string, seq chan ds.Planet) {
		remote := d.demand(name)
		defer func() {
			go func() {
				d.release(remote)
			}()
		}()
		res, err := http.Get(fmt.Sprintf("http://%s/planet/%s/%s", remote, name, time))
		var planet ds.Planet
		err = json.Unmarshal(utils.ReadData(res.Body), &planet)
		if err != nil {
			log.Printf("取得數據無效。%s\n", err.Error())
			return
		}
		seq <- planet
	}
	return d.dispatcherBase._dispatch(time, planets, getPlanet)
}
