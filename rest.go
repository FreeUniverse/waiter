package main

import (
	"common/utils"
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

type vinfo struct {
	addr    string
	planets []string
}

func startRESTServer(laddr string) {
	http.HandleFunc("/planet_list", hPlanetList) //星球列表
	http.HandleFunc("/info/", hInfo)             //星球信息
	http.HandleFunc("/planet/", hPlanet)         //某一星球的位置信息
	http.HandleFunc("/universe/", hUniverse)     //某時刻（URL下一部分）的星球（GET參數，空則全部）信息
	err := http.ListenAndServe(laddr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func hPlanetList(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		planets := idispatcher.PlanetList()
		ret, err := json.Marshal(planets)
		if err != nil {
			panic(err)
		}
		w.Write(ret)
	} else {
		log.Printf("非預期方法。請求：%s %s。來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}

func hInfo(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		path := strings.Split(strings.Trim(req.URL.EscapedPath(), "/"), "/")
		name := path[1]
		planets := idispatcher.Info(name)
		ret, err := json.Marshal(planets)
		if err != nil {
			panic(err)
		}
		w.Write(ret)
	} else {
		log.Printf("非預期方法。請求：%s %s。來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}

func hPlanet(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		path := strings.Split(strings.Trim(req.URL.EscapedPath(), "/"), "/")
		time := path[2]
		name := path[1]
		if utils.IsValidTime(time) {
			pInfo := idispatcher.Dispatch(time, []string{name})
			data, err := json.Marshal(pInfo)
			if err != nil {
				panic(err)
			}
			w.Write(data)
		} else {
			log.Printf("無效時間。請求：%s，來自%s", req.URL)
		}
	} else {
		log.Printf("非預期方法。請求：%s %s，來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}

func hUniverse(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		time := req.URL.EscapedPath()[10:]
		if utils.IsValidTime(time) {
			pInfos := idispatcher.Planets()
			planets := make([]string, len(pInfos))
			for i, info := range pInfos {
				planets[i] = info.Name
			}
			res := idispatcher.Dispatch(time, planets)
			data, err := json.Marshal(res.Planets)
			if err != nil {
				panic(err)
			}
			w.Write(data)
		} else {
			log.Printf("無效時間。請求：%s，來自%s", req.URL)
		}
	} else {
		log.Printf("非預期方法。請求：%s %s，來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}
